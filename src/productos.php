<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="css/shop.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet"type="text/css" href="css/style.css">
  <script src="js/jquery-3.3.1.min.js">

  </script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Adidas Shop</title>

</head>

<body>
  <div class="menu">
    <div class="adidas"><img src="img/adidas-logo.jpg" class="Icono" alt="Icono de Adidas "></div>
    <ul >

      <ul >
        <?php if(!isset($_SESSION['usuario'])){?>
                <li><a href="index.php"> Home </a></li>
                <li><a href="#s"> Shop </a></li>
                <li><a href="#bu"> Buy Now </a></li>
               <li><a href="productos.php"> Productos </a></li>

        <?php }

              else{ ?>
                  <li><a href="index2.php"> Home </a></li>
                  <li><a href="productos.php"> Modificaciones </a></li>
        <?php } ?>

        </ul>

    </ul>

    <div class="carrito"><img src="img/shopping-cart.png" class="Compra" alt="carrito"></div>
  </div>

  <?php
    if(!isset($_SESSION['usuario'])){
    ?>
      <P ALIGN="right"><a  class="boton" href="entrada.php"><b>Iniciar sesión</b></a></P>
    <?php
    }
    else{
    ?>
      <P ALIGN="right"><a class="boton" href="logout.php">Cerrar sesión</a></P>
    <?php
    }
    ?>
  <?php if(isset($_SESSION['usuario'])){?>
          <P ALIGN="right"><a id="boton" class="fancybox fancybox.iframe" href="alta.php">Alta de Productos</a></P>
  <?php }?>

<section>


	<?php
		include 'conexion.php';
    for ($i=1; $i < 4 ; $i++) {
		$re=mysql_query("SELECT * FROM category inner join product on product.id_category=category.id where id_category=$i")or die(mysql_error());
    $j=0;
    while ($f=mysql_fetch_array($re)) {
      if ($j==0) {?>
        <div class= "Encabezado" >
              <div class="titulo2"> <?php echo $f['description'];?></div>
              <div class="subtitulo2">
                  ADDIDAS SHOP
              </div>
              <div class="linea2"></div>
       </div>

    <?php
      $j=1;
    }

		?>
          <div class="producto">
          <center>
          <img src="img/<?php echo $f['ruta'];?>"><br>
          <div class="especificaciones">
          <div class="titulo">	<span><?php echo $f['name'];?></span><br></div>
          <div class="descrpcion">  <span><?php echo $f['description'];?></span><br></div>
          <div class="Producto_precio"><span>$<?php echo $f['price'];?></span><br></div>
          <?php
      if(isset($_SESSION['usuario'])){
        echo "<td><a class='fancybox fancybox.iframe' href='eliminar.php?id=".$f['id']."'>Eliminar</a></td>";
        echo "<td><a class='fancybox fancybox.iframe' href='actualizar.php?id=".$f['id']."&nombre=".$f['name']."&precio=".$f['price']."&id_categoria=".$f['id_category']."&ruta=".$f['ruta']."'>Actualizar</a></td>";
      }
      ?>


      </div>
			</center>
		</div>

	<?php
      }
		}
	?>




	</section>
</body>
<!-- Add mousewheel plugin (this is optional) -->
  <script src="lib/jquery-1.7.1.min.js" type="text/javascript"></script><!-- carga jQuery -->
	<script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5" media="screen" />

	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('.fancybox').fancybox();

			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});


			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});


			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'registro.php',
					type : 'iframe',
					padding : 5,

				});
			});

		});
	</script>

</html>
