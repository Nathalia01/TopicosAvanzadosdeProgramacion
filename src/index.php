<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="css/shop.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet"type="text/css" href="css/style.css">
  <script src="js/jquery-3.3.1.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Adidas Shop</title>

</head>

<body>
  <div class="menu">
    <div class="adidas"><img src="img/adidas-logo.jpg" class="Icono" alt="Icono de Adidas "></div>
    <ul >

      <li><a href="index.php"> Home </a></li>
      <li><a href="#s"> Shop </a></li>
      <li><a href="#bu"> Buy Now </a></li>
      <li><a href="productos.php"> Productos </a></li>

    </ul>



    <div class="carrito"><img src="img/shopping-cart.png" class="Compra" alt="carrito"></div>
  </div>
  <?php
  if(!isset($_SESSION['usuario'])){
  ?>
  <P ALIGN="right"><a  class="boton" href="entrada.php"><b>Iniciar sesión</b></a></P>
  <?php
  }
  else{
  ?>
  <P ALIGN="right"><a class="boton" href="logout.php">Cerrar sesión</a></P>
  <?php
  }
  ?>

  <div  class= "slider"  >
    <ul>
      <li><img src="img/banner.jpg" ></li>
      <li><img src="img/imagen3.jpg" ></li>
      <li><img src="img/banner.jpg" ></li>
      <li><img src="img/imagen4.jpg" ></li>

    </ul>
  <ol id="indicator">
    <li cuadrito="1"><i class="material-icons">crop_square</i></li>
    <li cuadrito="2"><i class="material-icons">crop_square</i></li>
    <li cuadrito="3"><i class="material-icons">crop_square</i></li>
    <li cuadrito="4"><i class="material-icons">crop_square</i></li>
  </ol>
  <div id="left"><i class="material-icons">chevron_left</i></div>
  <div id="right"><i class="material-icons">chevron_right</i></div>
  </div>

  <div class= "Encabezado" >
        <div class="titulo1">Trending</div>
        <div class="subtitulo">
             Most Trending Clothes
        </div>
        <div class="linea"></div>
</div>

<section>

	<?php
		include 'conexion.php';
		$re=mysql_query("SELECT * FROM category inner join product on product.id_category=category.id limit 4")or die(mysql_error());
		while ($f=mysql_fetch_array($re)) {
		?>


			<div class="producto">
			<center>
			<img src="img/<?php echo $f['ruta'];?>"><br>
      <div class="especificaciones">
      <div class="titulo">	<span><?php echo $f['name'];?></span><br></div>
      <div class="descrpcion">  <span><?php echo $f['description'];?></span><br></div>
      <div class="Producto_precio"><span>$<?php echo $f['price'];?></span><br></div>
      </div>
			</center>
		</div>

	<?php

		}
	?>




	</section>






<script src="js/script.js"></script>
</body>
</html>
