var contador=1;
var loop= false;
$("#indicator li").click(function(){
    var cuadrito = $(this).attr("cuadrito");

    $(".slider ul li").css({"display":"none"});
    $(".slider ul li:nth-child("+ cuadrito +")").fadeIn();
    $("#indicator li").css({"opacity":".5"});
    $(this).css({"opacity":"1"});

    loop = true;
    contador = cuadrito;
});

function avanzar() {
  if(contador >= 4){
    contador = 1;
  }else {
      contador++;
  }
  $(".slider ul li").css({"display":"none"});
  $(".slider ul li:nth-child("+ contador +")").fadeIn();
  $("#indicator li").css({"opacity":".5"});
  $(".slider li:nth-child("+ contador +")").css({"opacity":"1"});

}

$("#right i").click(function(){
  avanzar();
  loop = true;
});

$("#left i").click(function(){

  if(contador <= 1){
    contador = 4;
  }else {
      contador--;
  }
  $(".slider ul li").css({"display":"none"});
  $(".slider ul li:nth-child("+ contador +")").fadeIn();
  $("#indicator li").css({"opacity":".5"});
  $(".slider li:nth-child("+ contador +")").css({"opacity":"1"});
  loop = true;

});


setInterval(function(){
  if(loop){
    loop=false;
  }else {
    avanzar();
  }


},3000);
